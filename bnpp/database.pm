use strict;
use warnings;

# Namespace
package bnpp::database;

# Nimsoft librairies !
use lib "D:/apps/Nimsoft/perllib";
use lib "D:/apps/Nimsoft/Perl64/lib/Win32API";
use Nimbus::API;
use Nimbus::PDS;
use Nimbus::CFG;

use Term::ANSIColor qw(:constants);
use Win32::Console::ANSI;

use Data::Dumper;
$Data::Dumper::Indent = 1;

sub new {
    my ($class,$SQLServer,$User,$Password,$Database) = @_;
    my $this = {
        ctx => DBI->connect("$SQLServer;UID=$User;PWD=$Password",{RaiseError => 1,AutoCommit => 1}),
        hub => undef,
        database => $Database
    };
    return bless($this,ref($class) || $class);
}

sub closePool {
    my ($this) = @_;
    $this->{ctx}->disconnect;
}

sub defined {
    my ($self) = @_;
    if(defined($self->{ctx})) {
        $self->{ctx}->do("USE $self->{database}");
        return 1;
    }
    return 0;
}

sub setHUB {
    my ($self,$hub) = @_;
    my ($id,$sth,$rows) = undef;

    $sth = $self->{ctx}->prepare("SELECT id FROM list_hubs WHERE hubname = ?");
    $sth->execute($hub->{name});
    $rows = $sth->rows;
    if($rows) {
        while(my @Arr = $sth->fetchrow_array) {
            $id = $Arr[0];
            last;
        }
    }
    $sth->finish();

    if($rows) {
        $sth = $self->{ctx}->prepare("UPDATE list_hubs SET domain=?,version=?,hubaddr=?,hubip=?,origin=?,ldap=?,uptime=?,started=?,tunnel=? WHERE hubname = ?");
        $sth->execute(
            $hub->{domain},
            $hub->{version},
            $hub->{addr},
            $hub->{ip},
            $hub->{origin},
            $hub->{ldap},
            $hub->{uptime},
            $hub->{started},
            $hub->{tunnel},
            $hub->{name}
        );
        $sth->finish();
        $self->{fk} = $id;
    }
    else {
        $sth = $self->{ctx}->prepare("INSERT INTO list_hubs (hubname,domain,version,hubaddr,hubip,origin,ldap,uptime,started,tunnel) VALUES (?,?,?,?,?,?,?,?,?,?)");
        $sth->execute(
            $hub->{name},
            $hub->{domain},
            $hub->{version},
            $hub->{addr},
            $hub->{ip},
            $hub->{origin},
            $hub->{ldap},
            $hub->{uptime},
            $hub->{started},
            $hub->{tunnel}
        );
        $sth->finish();
        my @rowID = $self->{ctx}->selectrow_array('SELECT @@IDENTITY');
        $self->{fk} = $rowID[0];
    }

    $self->{hub} = $hub;
    return 1;
}

sub getSQLRobots {
    my ($self) = @_;
    my $sth = $self->{ctx}->prepare("SELECT id,robotname FROM list_robots WHERE hubid = ?");
    $sth->execute($self->{fk});
    my %TempHash = ();
    while(my @ROBOTArray = $sth->fetchrow_array){
        $TempHash{$ROBOTArray[1]} = $ROBOTArray[0];
    }
    $sth->finish();
    return %TempHash;
}

sub setSecond {
    my ($self,$robot) = @_;
    my $DB  = $self->{ctx};
    my $DB2 = $self->{ctx2};

    ### PRIMARY
    my $sth_primary = $DB->prepare("SELECT * FROM list_hubs_primary WHERE robotid = ?");
    $sth_primary->execute($robot->{self_fk});

    if($sth_primary->rows) {
        my $primary_update = $DB2->prepare("UPDATE list_hubs_primary SET domain=?,name=?,robotname=?,ip=?,dns_name=?,port=? WHERE robotid=?");
        $primary_update->execute(
            $robot->{phub_domain},
            $robot->{phub_name},
            $robot->{phub_robotname},
            $robot->{phub_ip},
            $robot->{phub_dns_name},
            $robot->{phub_port},
            $robot->{self_fk}
        );
        $primary_update->finish();
    }
    else {
        my $primary_insert = $DB2->prepare("INSERT INTO list_hubs_primary (robotid,domain,name,robotname,ip,dns_name,port) VALUES (?,?,?,?,?,?,?)");
        $primary_insert->execute(
            $robot->{self_fk},
            $robot->{phub_domain},
            $robot->{phub_name},
            $robot->{phub_robotname},
            $robot->{phub_ip},
            $robot->{phub_dns_name},
            $robot->{phub_port},
        );
        $primary_insert->finish();
    }
    $sth_primary->finish();

    ### SECONDARY
    my $sth_secondary = $DB->prepare("SELECT * FROM list_hubs_secondary WHERE robotid = ?");
    $sth_secondary->execute($robot->{self_fk});

    if($sth_secondary->rows) {
        my $secondary_update = $DB2->prepare("UPDATE list_hubs_secondary SET domain=?,name=?,robotname=?,ip=?,dns_name=?,port=? WHERE robotid=?");
        $secondary_update->execute(
            $robot->{shub_domain},
            $robot->{shub_name},
            $robot->{shub_robotname},
            $robot->{shub_ip},
            $robot->{shub_dns_name},
            $robot->{shub_port},
            $robot->{self_fk}
        );
        $secondary_update->finish();
    }
    else {
        my $secondary_insert = $DB2->prepare("INSERT INTO list_hubs_secondary (robotid,domain,name,robotname,ip,dns_name,port) VALUES (?,?,?,?,?,?,?)");
        $secondary_insert->execute(
            $robot->{self_fk},
            $robot->{shub_domain},
            $robot->{shub_name},
            $robot->{shub_robotname},
            $robot->{shub_ip},
            $robot->{shub_dns_name},
            $robot->{shub_port}
        );
        $secondary_insert->finish();
    }
    $sth_secondary->finish();

    return 1;
}

sub setROBOT {
    my ($self,$robot,$RobotsSQL,$robotsuccess) = @_;
    my $sth = undef;

    if( exists( $RobotsSQL->{$robot->{name}} ) ) {
        $sth = $self->{ctx}->prepare("UPDATE list_robots SET hubid=?,robotip=?,domain=?,origin=?,source=?,uptime=?,started=?,os_major=?,os_minor=?,os_version=?,os_description=?,os_user1=?,os_user2=?,status=?,success=? WHERE robotname = ?");
        $sth->execute(
            $self->{fk},
            $robot->{ip},
            $robot->{domain},
            $robot->{origin},
            $robot->{source},
            $robot->{uptime},
            $robot->{started},
            $robot->{os_major},
            $robot->{os_minor},
            $robot->{os_version},
            $robot->{os_description},
            $robot->{os_user1},
            $robot->{os_user2},
            $robot->{status},
            $robotsuccess,
            $robot->{name}
        );
        return $RobotsSQL->{ $robot->{name} };
    }
    else {
        $sth = $self->{ctx}->prepare("INSERT INTO list_robots (hubid,robotname,robotip,domain,origin,source,robot_device_id,uptime,started,os_major,os_minor,os_version,os_description,os_user1,os_user2,status,success) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $sth->execute(
            $self->{fk},
            $robot->{name},
            $robot->{ip},
            $robot->{domain},
            $robot->{origin},
            $robot->{source},
            $robot->{device_id},
            $robot->{uptime},
            $robot->{started},
            $robot->{os_major},
            $robot->{os_minor},
            $robot->{os_version},
            $robot->{os_description},
            $robot->{os_user1},
            $robot->{os_user2},
            $robot->{status},
            $robotsuccess
        );

        my @rowID = $self->{ctx}->selectrow_array('SELECT @@IDENTITY');
        return $rowID[0];
    }
}

sub deleteAll {
    my ($self,$RobotFK) = @_;
    $self->{ctx}->do("DELETE FROM list_probes WHERE robotid = ".$RobotFK." ");
}

sub probeSUCCESS {
    my ($self,$ProbeFK,$RC) = @_;
    $self->{ctx}->do("UPDATE list_probes SET success = ".$RC." WHERE id = '".$ProbeFK."'");
}

sub setPROBE {
    my ($self,$probe,$robotid,$PROBE_RC) = @_;

    my $probe_insert = $self->{ctx}->prepare("INSERT INTO list_probes (robotid,name,pid,active,command,arguments,timespec,times_activated,times_started,last_action,last_started,process_state,pkg_name,pkg_version,pkg_build,success) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $probe_insert->execute(
        $robotid,
        $probe->{name},
        $probe->{pid},
        $probe->{active},
        $probe->{command},
        $probe->{arguments},
        $probe->{timespec},
        $probe->{times_activated},
        $probe->{times_started},
        $probe->{last_action},
        $probe->{last_started},
        $probe->{process_state},
        $probe->{pkg_name},
        $probe->{pkg_version},
        $probe->{pkg_build},
        $PROBE_RC
    );
    $probe_insert->finish();

    my @rowID = $self->{ctx}->selectrow_array('SELECT @@IDENTITY');
    return $rowID[0];
}

sub setPACKAGE {
    my ($self,$pkg,$robotid) = @_;
    my $sth = $self->{ctx}->prepare("INSERT INTO list_robots_pkgs (robotid,name,description,version,build,date,install_date) VALUES(?,?,?,?,?,?,?)");
    $sth->execute(
        $robotid,
        $pkg->{name},
        $pkg->{description},
        $pkg->{version},
        $pkg->{build},
        $pkg->{date},
        $pkg->{install_date}
    );
    $sth->finish();
}

1;
