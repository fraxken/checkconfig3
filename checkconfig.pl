# Require librairies!
use strict;
use warnings;
use lib "D:/apps/Nimsoft/perllib";
use lib "D:/apps/Nimsoft/Perl64/lib/Win32API";
use Nimbus::API;
use Nimbus::CFG;
use Nimbus::PDS;
use Data::Dumper;
use DBI;
use File::Copy;
use File::Path 'rmtree';

use bnpp::main;
use bnpp::hub;
use bnpp::robot;
use bnpp::probe;
use bnpp::package;
use bnpp::database;
use bnpp::log;

use Term::ANSIColor qw(:constants);
use Win32::Console::ANSI;

# Globals Variable
my $GBL_ProbeName = "checkconfig";
my $GBL_ET = time();
my $LOGFILE = "$GBL_ProbeName.log";

unlink($LOGFILE);

# Parse and get CFG informations!
my $CFG                     = Nimbus::CFG->new("$GBL_ProbeName.cfg");
my $STR_DOMAIN              = $CFG->{"setup"}->{"domain"} || "NMS-PROD";
my $INT_LOGLEVEL            = $CFG->{"setup"}->{"loglevel"} || 3;
my $CACHE_DAY               = $CFG->{"setup"}->{"cache_delay"} || 5;
my $BOOL_GETConfiguration   = $CFG->{"monitoring"}->{"get_configuration"} || 1;
my $BOOL_GETPackages        = $CFG->{"monitoring"}->{"get_packages"} || 0;
my @ARR_Probes              = split(",",$CFG->{"monitoring"}->{"probes"});
my %Hash_Probes             = map { $_ => 1 } @ARR_Probes;
my $BOOL_RestrictProbes     = scalar(@ARR_Probes) ? 1 : 0;

bnpp::log::console("########################################",5);
bnpp::log::console("### Execution start at ".localtime()." ###",5);
bnpp::log::console("########################################",5);

my $rc = nimLogin("administrator","nim76prox");
if(not $rc) {
    die "Unable to connect to the nimsoft HUB !\n";
}

my $IM = new bnpp::main($GBL_ProbeName,$STR_DOMAIN,1);
my $DATE_ExecutionStartTime = $IM->getDate();
$bnpp::log::level = $INT_LOGLEVEL;

# Check old directory
$IM->createDirectory("output/");
{
    my $exec_annee  = substr($DATE_ExecutionStartTime,0,4);
    my $exec_month  = substr($DATE_ExecutionStartTime,4,2);
    my $exec_day    = substr($DATE_ExecutionStartTime,6,2);
    my $regular_day = 31;

    my @removeDirectory = ();
    opendir(DIR,'output/');
    my @directory = readdir(DIR);
    foreach(@directory) {
        next if $_ eq '.' || $_ eq '..';
        my $annee   = substr($_,0,4);
        my $month   = substr($_,4,2);
        my $day     = substr($_,6,2);

        if($exec_annee > $annee) {
            push(@removeDirectory,$_);
            next;
        }
        else {
            my $calc = $exec_day - $CACHE_DAY;
            if( $calc <= 0 ) {
                my $average_day = $regular_day - abs($calc);
                if( $average_day > $day && $month != $exec_month ) {
                    push(@removeDirectory,$_);
                    next;
                }
            }
            elsif( $calc > $day ) {
                push(@removeDirectory,$_);
                next;
            }
        }
    }

    foreach(@removeDirectory) {
        bnpp::log::print($LOGFILE,"Remove old output directory => $_",2);
        bnpp::log::console(YELLOW."Remove old output directory => $_",2);
        rmtree("output/$_");
    }
}

$IM->createDirectory("output/$DATE_ExecutionStartTime/");
bnpp::log::print($LOGFILE,"Create directory => ".YELLOW."output/$DATE_ExecutionStartTime/",3);
bnpp::log::console("Create directory => output/$DATE_ExecutionStartTime/",3);

my $DB;
{
    my $DB_User         = $CFG->{"CMDB"}->{"sql_user"};
    my $DB_Password     = $CFG->{"CMDB"}->{"sql_password"};
    my $DB_SQLServer    = $CFG->{"CMDB"}->{"sql_host"};
    my $DB_Database     = $CFG->{"CMDB"}->{"sql_database"};
    $DB = new bnpp::database($DB_SQLServer,$DB_User,$DB_Password,$DB_Database);
    if(not $DB->defined()) {
        bnpp::log::print($LOGFILE,"Unable to connect to the database!",1);
        bnpp::log::console(RED."Unable to connect to the database!",1);
    }
    else {
        bnpp::log::print($LOGFILE,"Connection to the database successfully!",3);
        bnpp::log::console(GREEN."Connection to the database successfully!",3);
    }
}

#
# Main Fonction
#
sub FCN_Main {
    my $HubRobot = $IM->Get_LocalHub();
    if(not $HubRobot) {
        bnpp::log::print($LOGFILE,"Unable to get hub informations!",1);
        bnpp::log::console(RED."Unable to get hub informations!",1);
        return 0;
    }
    if(not $DB->setHUB($HubRobot)) {
        bnpp::log::print($LOGFILE,"Failed to insert hub into the database!",1);
        bnpp::log::console(RED."Failed to insert hub into the database!",1);
        return 0;
    }


    my @RobotsArray = $IM->Get_LocalArrayRobots(2); # Retry two time if fail.
    if(scalar @RobotsArray == 0) {
        bnpp::log::print($LOGFILE,"Failed to get the list of robots",1);
        bnpp::log::console(RED."Failed to get the list of robots",1);
        return 0;
    }

    bnpp::log::console(YELLOW."Get SQL Robots!.",3);
    bnpp::log::print($LOGFILE,"Get SQL Robots!.",3);
    my %RobotsSQL = $DB->getSQLRobots();
    my @RobotArray_OK = ();

    # Statistiques informations
    my $robot_count     = 0;
    my $robot_fail      = 0;
    my $robot_down      = 0;
    my $setfail         = 0;
    my $gethub_fail     = 0;
    my $packages_fail   = 0;

    #
    # While 1 for inserting robots informations!
    #
    $DB->{ctx}->begin_work;
    foreach my $IRobot (@RobotsArray) {
        bnpp::log::console("---------------------------------------->",5);
        bnpp::log::console("Start processing robot ".GREEN."$IRobot->{name}".RESET." [$robot_count]",5);
        bnpp::log::print($LOGFILE,"Start processing robot $IRobot->{name} [$robot_count]",3);

        $IM->createDirectory("output/$DATE_ExecutionStartTime/$IRobot->{name}");
        $robot_count++;

        my $RC = 0;
        if($IRobot->{status} == 0) {

            $RC = $IRobot->Get_LocalInfo();
            if(!$RC) {
                $IRobot->setStatus(6);
                bnpp::log::console(YELLOW."[callback: get_info]".RESET.RED." callback fail! Set robot status to 6.",2);
                bnpp::log::print($LOGFILE,"[callback: get_info] callback fail! Set robot status to 6.",2);
                bnpp::log::console(YELLOW."[callback: gethubs]".RESET.RED." the callback is cancelled - Robot not reachable.",2);
                bnpp::log::print($LOGFILE,"[callback: gethubs] the callback is cancelled - Robot not reachable.",2);
                $gethub_fail++;
            }
            else {
                bnpp::log::console(YELLOW."[callback: get_info]".RESET." callback successfull!",3);
                bnpp::log::print($LOGFILE,"[callback: get_info] callback successfull!",3);

                if($IRobot->Get_LocalHub()) {
                    bnpp::log::console(YELLOW."[callback: gethubs]".RESET." successfully done!",3);
                    bnpp::log::print($LOGFILE,"[callback: gethubs] successfully done!",3);
                }
                else {
                    bnpp::log::console(YELLOW."[callback: gethubs]".RESET.RED." fail!",1);
                    bnpp::log::print($LOGFILE,"[callback: gethubs] fail!",1);
                    $gethub_fail++;
                }
            }

        }

        my $RobotFK = $DB->setROBOT($IRobot,\%RobotsSQL,$RC) || -1;
        if($RobotFK == -1) {
            $setfail++;
            next;
        }
        #$DB->setSecond($IRobot);
        $IRobot->{self_fk} = $RobotFK;
        bnpp::log::console("RobotFK equal $RobotFK",3);
        bnpp::log::print($LOGFILE,"RobotFK equal $RobotFK",3);

        if($RC) {
            bnpp::log::console(GREEN."Delete all probes/confs from the database for robotid =>".RESET.YELLOW." $RobotFK!",3);
            bnpp::log::print($LOGFILE,"Delete all probes/confs from the database for robotid => $RobotFK!",3);
            $DB->deleteAll($RobotFK);

            push(@RobotArray_OK,$IRobot);
        }
        else {
            $DB->probeSUCCESS($RobotFK,0);
            bnpp::log::console(RED."Robot => ".RESET.YELLOW."$IRobot->{name}".RESET.RED." is down!",2);
            bnpp::log::print($LOGFILE,"Robot => $IRobot->{name} is down!",2);
            $robot_down++;
        }

    }
    bnpp::log::console(RED."Commit all robots actions into the database!",3);
    bnpp::log::print($LOGFILE,"Commit all robots actions into the database!",3);
    $DB->{ctx}->commit();

    #
    # While2 for inserting probes, probes configuration & packages into the database.
    #
    my $count_robot_temp = 0;
    foreach my $IRobot (@RobotArray_OK) {
        $count_robot_temp++;
        bnpp::log::console("---------------------------------------->",5);
        bnpp::log::console("Start processing robot ".GREEN."$IRobot->{name}".RESET." [$count_robot_temp]",5);
        bnpp::log::print($LOGFILE,"Start processing robot $IRobot->{name} [$count_robot_temp]",3);

        # New chunk
        {
            my ($PROBE_RC,@ProbeArray) = $IRobot->Get_LocalArrayProbes();
            if(scalar(@ProbeArray) > 0 && $PROBE_RC) {

                my @ProbeOk = $IM->includeProbe(\@ProbeArray,\%Hash_Probes);
                if($BOOL_GETConfiguration) {
                    foreach my $Probe (@ProbeOk) {
                        my $ProbeFK = $DB->setPROBE($Probe,$IRobot->{self_fk},$PROBE_RC) || -1;
                        $DB->{ctx}->begin_work;
                        bnpp::log::console("Get CFG for => ".GREEN."$Probe->{name}",3);
                        bnpp::log::print($LOGFILE,"Get CFG for => $Probe->{name}",3);
                        if($Probe->Get_CFG("output/$DATE_ExecutionStartTime/$IRobot->{name}/") && $ProbeFK != -1) {
                            $Probe->parseCONF($DB,$ProbeFK);
                        }
                        else {
                            bnpp::log::console(RED."Failed to get CFG for =>".RESET.YELLOW." $Probe->{name}",1);
                            bnpp::log::print($LOGFILE,"Failed to get CFG for => $Probe->{name}",1);
                        }
                        $DB->{ctx}->commit();
                    }
                }

            }
            else {
                $DB->probeSUCCESS($IRobot->{self_fk},0);
                bnpp::log::console(RED."No probes or failed to get probes list!",1);
                bnpp::log::print($LOGFILE,"No probes or failed to get probes list!",1);
                $robot_fail++;
            }
        }

        bnpp::log::console("Processing ".GREEN."deployed package".RESET." on the robot!",3);
        bnpp::log::print($LOGFILE,"Processing deployed package on the robot!",3);
        my ($RC_PKG,@Packages) = $IRobot->Get_LocalPackages();
        if($RC_PKG) {

            bnpp::log::console("Delete all SQL packages on the robot...",3);
            bnpp::log::print($LOGFILE,"Delete all SQL packages on the robot...",3);
            my $sth_packages = $DB->{ctx}->prepare("DELETE FROM list_robots_pkgs WHERE robotid = ?");
            $sth_packages->execute($IRobot->{self_fk});
            $sth_packages->finish();

            bnpp::log::console(GREEN."=> Add packages into the database...",5);
            bnpp::log::print($LOGFILE,"Add packages into the database...",5);

            foreach my $pkg (@Packages) {
                bnpp::log::console("package ".GREEN."$pkg->{name}".RESET." to the database!",3);
                bnpp::log::print($LOGFILE,"package $pkg->{name} to the database!",3);
                $DB->setPACKAGE($pkg,$IRobot->{self_fk});
            }

            bnpp::log::console(GREEN."=> Deployed packages done!",5);
            bnpp::log::print($LOGFILE,"Deployed packages done!",5);
        }
        else {
            bnpp::log::console(RED."Failed to get packages!",2);
            bnpp::log::print($LOGFILE,"Failed to get packages!",2);
            $packages_fail++;
        }

    }

    bnpp::log::console("",5);
    bnpp::log::console(MAGENTA."<= Statistiques =>",5);
    bnpp::log::console("",5);
    bnpp::log::console("Robots total count => ".YELLOW."$robot_count",5);
    bnpp::log::console("Robots with probes KO => ".YELLOW."$robot_fail",5);
    bnpp::log::console("Robots gethubs fail (callback) => ".YELLOW."$gethub_fail",5);
    bnpp::log::console("Robot down (status 2) => ".YELLOW."$robot_down",5);
    bnpp::log::console("Packages fail (callback) => ".YELLOW."$packages_fail",5);

    bnpp::log::print($LOGFILE,"Robots total count => $robot_count",5);
    bnpp::log::print($LOGFILE,"Robots with probes KO => $robot_fail",5);
    bnpp::log::print($LOGFILE,"Robots gethubs fail (callback) => $gethub_fail",5);
    bnpp::log::print($LOGFILE,"Robot down (status 2) => $robot_down",5);
    bnpp::log::print($LOGFILE,"Packages fail (callback) => $packages_fail",5);

}

# Launch program
if(not FCN_Main()) {
    bnpp::log::console(RED."Checkconfig failed to achieve the work!",1);
    bnpp::log::print($LOGFILE,"Checkconfig failed to achieve the work!",1);
}

$DB->closePool();

# Copy the file log into the ouput directory
copy("$GBL_ProbeName.log","output/$DATE_ExecutionStartTime/$GBL_ProbeName.log") or warn "Failed to copy the log into the final outdir";

my $FINAL_TIME = sprintf("%.2f", time() - $GBL_ET);
my $Minute = sprintf("%.2f", $FINAL_TIME / 60);
bnpp::log::console(GREEN."\n\nFinal execution time = ".RESET.YELLOW."$FINAL_TIME second(s) [$Minute minutes]!",5);
bnpp::log::print($LOGFILE,"Final execution time = $FINAL_TIME second(s) [$Minute minutes]!",5);
bnpp::log::console("########################################",5);
bnpp::log::console("### Execution end at ".localtime()." ###",5);
bnpp::log::console("########################################",5);
